package parser

import (
	"errors"
	"io/ioutil"
	"math"
	"net/http"
	"strings"
	"time"

	"github.com/buger/jsonparser"
)

var errNoKripta = errors.New("No such kripta")

type Currency struct {
	ID    int
	Name  string
	Code  string
	Price float64
}

type Requestor struct {
	client http.Client
}

func NewClient() *Requestor {
	return &Requestor{client: http.Client{Timeout: 10 * time.Second}}
}

func (r Requestor) GetCurrencyByName(name string) (*Currency, error) {
	if name == "" {
		return nil, errNoKripta
	}
	resp, err := r.client.Get("https://api.coingecko.com/api/v3/coins/" + name)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	name, code, price, err := getFields(b)
	if err != nil {
		return nil, err
	}

	return &Currency{
		ID:    1,
		Name:  name,
		Code:  code,
		Price: price,
	}, nil
}

func (r Requestor) GetCurrencyList(n string) ([]Currency, error) {
	resp, err := r.client.Get("https://api.coingecko.com/api/v3/coins?per_page=" + n)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var currencies []Currency
	var errors []error
	var id = 1
	jsonparser.ArrayEach(b, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		name, code, price, err := getFields(value)
		if err != nil {
			errors = append(errors, err)
			return
		}

		res := Currency{
			ID:    id,
			Name:  name,
			Price: price,
			Code:  code,
		}

		currencies = append(currencies, res)
		id++
	})

	if len(errors) > 0 {
		return nil, errors[0]
	}

	return currencies, nil
}

func getFields(data []byte) (string, string, float64, error) {
	name, err := jsonparser.GetString(data, "name")
	if err != nil {
		return "", "", 0, err
	}

	code, err := jsonparser.GetString(data, "symbol")
	if err != nil {
		return "", "", 0, err
	}
	code = strings.ToUpper(code)

	price, err := jsonparser.GetFloat(data, "market_data", "current_price", "usd")
	if err != nil {
		return "", "", 0, err
	}

	return name, code, Round(price, 2), nil
}

func Round(x float64, prec int) float64 {
	var rounder float64
	pow := math.Pow(10, float64(prec))
	intermed := x * pow
	_, frac := math.Modf(intermed)
	if frac >= 0.5 {
		rounder = math.Ceil(intermed)
	} else {
		rounder = math.Floor(intermed)
	}

	return rounder / pow
}
