package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"cryptocurrencybot/parser"

	"github.com/Syfaro/telegram-bot-api"
)

var token string

const PREFIX = "getfucking"

func init() {
	if v, ok := os.LookupEnv("CRYPTOCURRENCY_TOKEN"); ok {
		token = v
	} else {
		panic("CRYPTOCURRENCY_TOKEN is not provided")
	}
}

func main() {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	requestor := parser.NewClient()

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		cmd := strings.ToLower(update.Message.Command())
		switch cmd {
		case "start":
			resp, err := requestor.GetCurrencyList("10")
			if err != nil {
				reportError(bot, update.Message.Chat.ID, err)
			}

			sendResponse(bot, update.Message.Chat.ID, resp)
		case "top50":
			resp, err := requestor.GetCurrencyList("50")
			if err != nil {
				reportError(bot, update.Message.Chat.ID, err)
			}

			sendResponse(bot, update.Message.Chat.ID, resp)
		default:
			if !strings.Contains(cmd, PREFIX) {
				return
			}
			t := strings.TrimPrefix(cmd, PREFIX)
			resp, err := requestor.GetCurrencyByName(t)
			if err != nil {
				reportError(bot, update.Message.Chat.ID, err)
			}

			sendResponse(bot, update.Message.Chat.ID, resp)
		}
	}
}

func reportError(bot *tgbotapi.BotAPI, chatID int64, err error) {
	bot.Send(tgbotapi.NewMessage(chatID, err.Error()))
}

func sendResponse(bot *tgbotapi.BotAPI, chatID int64, data interface{}) {
	var m string
	if item, ok := data.(*parser.Currency); ok {
		m = fmt.Sprintf("|%-d|%-6s|%-3s|%-5.2f$|\n", item.ID, item.Name, item.Code, item.Price)
	} else if items, ok := data.([]parser.Currency); ok {
		for _, v := range items {
			m += fmt.Sprintf("|%-d|%-6s|%-3s|%-5.2f$|\n", v.ID, v.Name, v.Code, v.Price)
		}
	} else {
		m = "Can't parse interface"
	}

	msg := tgbotapi.NewMessage(chatID, m)
	bot.Send(msg)
}
